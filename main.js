window.addEventListener("DOMContentLoaded", () => {
  "use strict";

  const url = "/data.json";
  const limit = 10;

  let filterState = {
    priceFilter: null,
    areaFilter: null,
    floorFilter: null,
    minPrice: null,
    maxPrice: null,
    minSize: null,
    maxSize: null,
    minPriceInputValue: null,
    maxPriceInputValue: null,
    minSizeInputValue: null,
    maxSizeInputValue: null,
    roomsNumber: null,
  };

  const addMoreBtn = document.querySelector(".apartments__add-more"),
    filterClearBtn = document.querySelector(".filter__clear"),
    filterRooms = document.querySelector(".filter__rooms"),
    wrap = document.querySelector(".apartments__table"),
    priceFilterBtn = document.querySelector(
      ".apartments__table-head .apartments__price"
    ),
    tableHeadBtns = document.querySelectorAll(".apartments__table-head .btn"),
    areaFilterBtn = document.querySelector(
      ".apartments__table-head .apartments__s"
    ),
    floorFilterBtn = document.querySelector(
      ".apartments__table-head .apartments__floor"
    );

  const getResource = async () => {
    let res = await fetch(url);
    if (!res.ok) {
      throw new Error(`Could not fetch ${url}` + `, received ${res.status}`);
    }

    let body = await res.json();

    return body.apartments;
  };

  getResource().then((apartments) => {
   
    changeFilterState(filterState, apartments);
  });

  const changeFilterState = (state, apartments) => {
    const prices = [],
      sizes = [];

    apartments.forEach((item) => {
      prices.push(+item.price.replace(/\s/g, ""));
      sizes.push(+item.size.replace(",", "."));
    });

    let set = new Set(apartments.map((item) => item.name.replace(/-.*/g, "")));
    let numbArr = Array.from(set);

    filterRooms.querySelectorAll(".filter__rooms-number").forEach((item) => {
      if (!numbArr.find((num) => num === item.dataset.number)) {
        item.setAttribute("data-disable", true);
      }
    });

    state.minPrice = Math.min.apply(null, prices);
    state.maxPrice = Math.max.apply(null, prices);
    state.minSize = Math.min.apply(null, sizes);
    state.maxSize = Math.max.apply(null, sizes);

    $("#min-price").val(state.minPrice);
    $("#max-price").val(state.maxPrice);
    $("#min-size").val(state.minSize);
    $("#max-size").val(state.maxSize);

    $("#filter__price").slider({
      range: true,
      min: state.minPrice,
      max: state.maxPrice,
      values: [state.minPrice, state.maxPrice],
      slide: function (event, ui) {
        $("#min-price").val(ui.values[0]);
        $("#max-price").val(ui.values[1]);
      },
    });
    $("#filter__size").slider({
      range: true,
      min: state.minSize,
      max: state.maxSize,
      values: [state.minSize, state.maxSize],
      slide: function (event, ui) {
        $("#min-size").val(ui.values[0]);
        $("#max-size").val(ui.values[1]);
      },
    });

    applyFilter(state);

    const mousedownListener = () => {
      let mouseupListener = () => {
        state.minSizeInputValue = +document.querySelector("#min-size").value;
        state.maxSizeInputValue = +document.querySelector("#max-size").value;
        state.minPriceInputValue = +document.querySelector("#min-price").value;
        state.maxPriceInputValue = +document.querySelector("#max-price").value;

        applyFilter(state);
        document.removeEventListener("mouseup", mouseupListener); // обязательно удалить после применения,тк обработчик действует на весь документ
      };
      document.addEventListener("mouseup", mouseupListener);
    };

    document.querySelectorAll("#filter__price span").forEach((item) => {
      item.addEventListener("mousedown", mousedownListener); // делаем через  mousedown, чтобы фильтр срабатывал, даже если мышка, зажавшая слайдер цены, ушла за поле фильтра
    });
    document.querySelectorAll("#filter__size span").forEach((item) => {
      item.addEventListener("mousedown", mousedownListener);
    });
    filterRooms.addEventListener("click", (e) => {
      if (e.target.hasAttribute("data-disable")) return;
      filterRooms.querySelectorAll(".filter__rooms-number").forEach((item) => {
        e.target !== item
          ? item.classList.remove("filter__rooms-number--active")
          : item.classList.add("filter__rooms-number--active");
      });
      state.roomsNumber = e.target.dataset.number;

      applyFilter(state);
    });

    priceFilterBtn.addEventListener("click", (e) => {
      state.areaFilter = null;
      state.floorFilter = null;
      state.priceFilter =
        state.priceFilter === "desk" || state.priceFilter === null
          ? "asc"
          : "desk";

      clearFilterClass(e);
      priceFilterBtn.setAttribute("data-filter", state.priceFilter);

      applyFilter(state);
    });
    floorFilterBtn.addEventListener("click", (e) => {
      state.areaFilter = null;
      state.priceFilter = null;
      state.floorFilter =
        state.floorFilter === "desk" || state.floorFilter === null
          ? "asc"
          : "desk";

      clearFilterClass(e);
      floorFilterBtn.setAttribute("data-filter", filterState.floorFilter);

      applyFilter(state);
    });
    areaFilterBtn.addEventListener("click", (e) => {
      state.priceFilter = null;
      state.floorFilter = null;
      state.areaFilter =
        state.areaFilter === "desk" || state.areaFilter === null
          ? "asc"
          : "desk";

      clearFilterClass(e);
      areaFilterBtn.setAttribute("data-filter", filterState.areaFilter);

      applyFilter(state);
    });
    filterClearBtn.addEventListener("click", (e) => {
      state.priceFilter = null;
      state.areaFilter = null;
      state.floorFilter = null;
      state.roomsNumber = null;
      state.maxPriceInputValue = state.maxPrice;
      state.minPriceInputValue = state.minPrice;
      state.maxSizeInputValue = state.maxSize;
      state.minSizeInputValue = state.minSize;

      $("#min-price").val(state.minPrice);
      $("#max-price").val(state.maxPrice);
      $("#min-size").val(state.minSize);
      $("#max-size").val(state.maxSize);
      $("#filter__price").slider({
        range: true,
        min: state.minPrice,
        max: state.maxPrice,
        values: [state.minPrice, state.maxPrice],
        slide: function (event, ui) {
          $("#min-price").val(ui.values[0]);
          $("#max-price").val(ui.values[1]);
        },
      });
      $("#filter__size").slider({
        range: true,
        min: state.minSize,
        max: state.maxSize,
        values: [state.minSize, state.maxSize],
        slide: function (event, ui) {
          $("#min-size").val(ui.values[0]);
          $("#max-size").val(ui.values[1]);
        },
      });
      filterRooms.querySelectorAll(".filter__rooms-number").forEach((item) => {
        item.classList.remove("filter__rooms-number--active");
      });
      
      clearFilterClass(e);

      applyFilter(state);
    });
  };
  function applyFilter(state) {
    document.querySelector(".filter").classList.add("blocked");
    getResource().then((apartments) => {
     
      let prop;

      if (state.roomsNumber) {
        apartments = apartments.filter((item) => {
          return item.name.replace(/-.*/g, "") === state.roomsNumber;
        });
      }

      if (state.minPriceInputValue) {
        apartments = apartments.filter((item) => {
          let tempPrice = +item.price.replace(/\s/g, "");
          return (
            tempPrice >= state.minPriceInputValue &&
            tempPrice <= state.maxPriceInputValue
          );
        });
      }

      if (state.minSizeInputValue) {
        apartments = apartments.filter((item) => {
          let tempSize = +item.size.replace(",", ".");
          return (
            tempSize >= state.minSizeInputValue &&
            tempSize <= state.maxSizeInputValue
          );
        });
      }
      if (state.priceFilter) {
        prop = state.priceFilter;
        apartments.forEach((item) => {
          item.sortValue = +item.price.replace(/\s/g, "");
        });
      }
      if (state.floorFilter) {
        prop = state.floorFilter;
        apartments.forEach((item) => {
          item.sortValue = +item.floor;
        });
      }
      if (state.areaFilter) {
        prop = state.areaFilter;
        apartments.forEach((item) => {
          item.sortValue = +item.size.replace(",", ".");
        });
      }

      if (prop === "asc")
        apartments.sort((a, b) => (a.sortValue > b.sortValue ? 1 : -1));
      if (prop === "desk")
        apartments.sort((a, b) => (a.sortValue > b.sortValue ? -1 : 1));

      wrap.innerHTML = "";

      let pos = 1;
     
      setTimeout(()=>{
        document.querySelector(".filter").classList.remove("blocked");
      }, 1000);
      
      appendItem(getArrRange(apartments, limit, pos));
    
      apartments.length > limit
        ? addMoreBtn.setAttribute("style", "display:block")
        : addMoreBtn.setAttribute("style", "display:none");

      addMoreBtn.addEventListener("click", () => {
        pos += limit;

      
        appendItem(getArrRange(apartments, limit, pos));
       
        if (pos >= apartments.length) {
          addMoreBtn.setAttribute("style", "display:none");
        }
      });
    });
  }

  function appendItem(arr) {
    arr.forEach((apartment) => {
      const { name, size, floor, price, img } = apartment;
      let item = document.createElement("div");

      item.className = "apartments__row";
      item.innerHTML = `		
          <div class="apartments__col apartments__img"><img src="${img}" alt=""></div>
          <div class="apartments__col apartments__name">${name}</div>
          <div class="apartments__col apartments__s">${size}</div>
          <div class="apartments__col apartments__floor">${floor} <span>&nbsp;из&nbsp;17</span><span class="hidden-l"> этаж</span></div>
          <div class="apartments__col apartments__price">${price}</div>`;
      wrap.appendChild(item);
    });

  }
  function clearFilterClass(target) {
    tableHeadBtns.forEach((item) => {
      if (target !== item) {
        item.removeAttribute("data-filter");
      }
    });
  }
  function getArrRange(arr, range, pos) {
    let start = pos - 1;
    let end = start + range;

    return arr.slice(start, end);
  }
  function trackScroll() {
    let scrolled = window.pageYOffset;
    let coords = document.documentElement.clientHeight;

    scrolled < coords
      ? document.querySelector(".go-top").classList.add("hidden")
      : document.querySelector(".go-top").classList.remove("hidden");
  }

  document.querySelector(".go-top").onclick = () => {
    window.scrollTo(pageYOffset, 0);
  };
  window.addEventListener("scroll", trackScroll);
});
